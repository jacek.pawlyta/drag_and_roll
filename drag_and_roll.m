% Matlab 2011a script 
% DRAG & ROLL
% Draw how drag force and rolling resistance depends on a car velocity
%
% author: Jacek Pawlyta
% date: 2019-12-02
% version: 1.0
% licence:  GPL 2.0

% lets create a car velocities matrix V
% velocity is expressed in km/h 
V = (0:10:140);

% lets define a car front area, value in m^2
area = 1;

% free fall accelleraion on the Earth in m/s^2
g = 9.81;

% define drag force coefficient cx for a car
cx = 0.3;

% define mass of a car in kg
m = 1250;

% lets calculate sum of the drag force (the first part of the sum) 
% and the rolling resistance for a given car velocity
% F is expressed in N
F = m*g/1000*(12+0.0006*V.^2) + 0.047*cx*area*V.^2;

plot(V,F)
xlabel('Car velocity, km/h');
ylabel('Drag force + rolling resistance, N');


